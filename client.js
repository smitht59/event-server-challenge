const net = require('net');
const { hostname, port } = require('./config');

const client = net.createConnection({ host: hostname, port }, () => {
  console.log('client connected');
});

client.on('data', (data) => {
  console.log(data.toString());
});
