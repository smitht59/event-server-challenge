const http = require('http');
const { hostname, port } = require('./config');

/**
 * set of currently connected sockets.
 */
const sockets = new Set();

const server = http.createServer();

/**
 * On a request event, process the event and broadcast to all connections
 */
server.on('request', (req, res) => {
  if (req.method !== 'POST') {
    res.statusCode = 500;
    res.end('Invalid Method');
  } else {
    let event;
    req.on('data', (data) => {
      event = data.toString();
      sockets.delete(req.socket);
    });
    req.on('end', () => {
      sockets.forEach((socket) => {
        socket.write(event);
      });
      console.log(`Broadcast event to ${sockets.size} clients:`);
      console.log(event, '\n');
      res.end('Event Received');
    });
  }
});

/**
 * On a new connection, add to the sockets set and register a close event handler to delete the socket from the set
 */
server.on('connection', (socket) => {
  console.log('Client connected');
  socket.on('close', (hadError) => {
    if (hadError) console.log('error');
    console.log('Client disconnected');
    sockets.delete(socket);
  });
  socket.setKeepAlive(true, 100);
  sockets.add(socket);
});

server.listen(port, hostname, () => { console.log(`Listening at http://${hostname}:${port}/`); });
